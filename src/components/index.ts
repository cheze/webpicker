import Vue from 'vue'
import NumberField from './NumberField.vue'
import EventTools from './EventTools.vue'
import EventDescription from './EventDescription.vue'
import ColorPicker from './ColorPicker.vue'
import KeybindingField from './KeybindingField.vue'
import ProgressBar from './ProgressBar.vue'
import StationRadiusSelector from './StationRadiusSelector.vue'
import FirstMotion from './FirstMotion.vue'

Vue.component('NumberField', NumberField)
Vue.component('EventTools', EventTools)
Vue.component('EventDescription', EventDescription)
Vue.component('ColorPicker', ColorPicker)
Vue.component('KeybindingField', KeybindingField)
Vue.component('ProgressBar', ProgressBar)
Vue.component('StationRadiusSelector', StationRadiusSelector)
Vue.component('FirstMotion', FirstMotion)
